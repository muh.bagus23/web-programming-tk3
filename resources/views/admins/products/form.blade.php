@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(\Request::route()->getName() == 'admin.product.create')
                <div class="card-header">{{ __('Create Product') }}</div>
                @else
                <div class="card-header">{{ __('Edit Product '.$product->name) }}</div>
                @endif

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="m-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(session('msg'))
                    <div class="alert alert-info">
                        {!! session('msg') !!}
                    </div>
                    @endif

                    @if(\Request::route()->getName() == 'admin.product.create')
                    <form method="POST" action="{{ route('admin.product.create') }}" enctype="multipart/form-data">
                    @else
                    <form method="POST" action="{{ route('admin.product.edit', $product->id) }}" enctype="multipart/form-data">
                        @method('PUT')
                    @endif

                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input type="text" required name="name" value="{{ old('name') ?? ($product->name ?? null) }}" class="form-control @error('name') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="capital_price" class="col-md-4 col-form-label text-md-end">{{ __('Capital Price') }}</label>

                            <div class="col-md-6">
                                <input type="number" required name="capital_price" value="{{ old('capital_price') ?? ($product->capital_price ?? null) }}" class="form-control @error('capital_price') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="selling_price" class="col-md-4 col-form-label text-md-end">{{ __('Selling Price') }}</label>

                            <div class="col-md-6">
                                <input type="number" required name="selling_price" value="{{ old('selling_price') ?? ($product->selling_price ?? null) }}" class="form-control @error('selling_price') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="stock" class="col-md-4 col-form-label text-md-end">{{ __('Stock') }}</label>

                            <div class="col-md-6">
                                <input type="number" required name="stock" value="{{ old('stock') ?? ($product->stock ?? null) }}" class="form-control @error('stock') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="type" class="col-md-4 col-form-label text-md-end">{{ __('Type') }}</label>

                            <div class="col-md-6">
                                <input type="text" required name="type" value="{{ old('type') ?? ($product->type ?? null) }}" class="form-control @error('type') is-invalid @enderror" />
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="description" class="col-md-4 col-form-label text-md-end">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <textarea name="description" class="form-control @error('description') is-invalid @enderror">{{ old('description') ?? ($product->description ?? null) }}</textarea>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="product_image_url" class="col-md-4 col-form-label text-md-end">{{ __('Image') }}</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control @error('product_image_url') is-invalid @enderror" name="product_image_url" <?=(\Request::route()->getName() == 'admin.product.create' ? 'required' : null)?> accept="image/png, image/jpeg, image/jpg" />
                                <img src="{{ $product->product_image_url ?? null }}" class="img-fluid my-2" />
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <a class="btn btn-secondary" href="{{ route('admin.product.index') }}">Cancel</a>
                                @if(\Request::route()->getName() == 'admin.product.create')
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                                @else
                                <button type="submit" class="btn btn-warning">
                                    {{ __('Update') }}
                                </button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection