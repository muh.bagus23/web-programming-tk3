@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-4">
                <div class="card-header">{{ __('Welcome '.auth()->user()->name) }}</div>

                <div class="card-body">
                    This is Dashboard...
                </div>
            </div>
        </div>
    </div>
</div>
@endsection