@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <section class="h-100 gradient-custom">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-lg-10 col-xl-8">
                <div class="card" style="border-radius: 10px;">
                <div class="card-header px-4 py-5">
                    <h5 class="mb-0">Order From <span>{{ auth()->user()->name }}</span></h5>
                </div>
                <div class="card-body p-4">
                    <div class="d-flex justify-content-between align-items-center mb-4">
                    <p class="lead fw-normal mb-0">Receipt</p>
                    <p class="small mb-0">
                        Status : 
                        @if($order->status == 'waiting_confirmation')
                        Waiting Confirmation
                        @elseif($order->status == 'completed')
                        Completed
                        @elseif($order->status == 'rejected')
                        Rejected
                        @endif
                    </p>
                    </div>
                    <div class="card shadow-0 border mb-4">
                        <div class="card-body">
                            @foreach($order->items as $item)
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="{{ $item->product->product_image_url }}"
                                    class="img-fluid" alt="Phone">
                                </div>
                                <div class="col-md-6 text-center d-flex justify-content-center align-items-center">
                                    <p class="mb-0">{{ $item->product->name }}</p>
                                </div>
                                <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                                    <p class="mb-0 small">Qty: {{ $item->qty }}</p>
                                </div>
                                <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                                    <p class="mb-0 small">{{ 'Rp'.number_format($item->price, 0, ',', '.') }}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="d-flex justify-content-between pt-2">
                    <p class="fw-bold mb-0">Order Details</p>
                    <p class="mb-0"><span class="fw-bold me-4">Total</span> {{ 'Rp'.number_format($order->total_price, 0, ',', '.') }}</p>
                    </div>

                    <div class="d-flex justify-content-between pt-2">
                    <p class="mb-0">Invoice Number : {{ $order->invoice_code }}</p>
                    </div>

                    <div class="d-flex justify-content-between">
                    <p class="mb-0">Invoice Date : {{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y H:i:s') }}</p>
                    </div>
                    <hr>
                    <div class="d-flex">
                        <a href="{{ route('admin.order.index') }}" class="btn btn-secondary">Back</a>
                        @if($order->status == 'waiting_confirmation')
                            <a onclick="event.preventDefault(); if(confirm('Are you sure want to reject this?')) document.getElementById('reject-form-{{ $order->id }}').submit();" class="ms-auto btn btn-danger">Reject</a>
                            <form action="{{ route('admin.order.reject') }}" method="POST" id="reject-form-{{ $order->id }}" style="display: none;">
                                @csrf
                                <input type="hidden" value="{{ $order->id }}" name="id">
                            </form>

                            <a onclick="event.preventDefault(); if(confirm('Are you sure want to confirm this?')) document.getElementById('confirm-form-{{ $order->id }}').submit();" class="ms-2 btn btn-success">Confirm</a>
                            <form action="{{ route('admin.order.confirm') }}" method="POST" id="confirm-form-{{ $order->id }}" style="display: none;">
                                @csrf
                                <input type="hidden" value="{{ $order->id }}" name="id">
                            </form>
                        @endif
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </section>
    </div>
</div>
@endsection
