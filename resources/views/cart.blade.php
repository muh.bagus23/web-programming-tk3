@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Cart') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        @php
                            $total_price = 0
                        @endphp
                        @foreach($carts as $cart)
                        <div class="col-12 mb-2">
                            <div class="d-flex align-items-center" style="border: 1px solid #bbb; border-radius: 4px;">
                                <div class="col-1">
                                    <img class="img-fluid" src="{{ $cart->product->product_image_url }}" />
                                </div>
                                <div class="col-8 px-3">
                                    {{ $cart->product->name }}
                                </div>
                                <div class="col-1 text-center">
                                    x{{ $cart->qty }}
                                </div>
                                <div class="col-2 text-end pe-3">
                                    {{ 'Rp'.number_format($cart->product->selling_price, 0, ',', '.') }}
                                </div>
                            </div>
                        </div>

                        @php
                            $total_price += ($cart->product->selling_price * $cart->qty)
                        @endphp
                        @endforeach
                        <form action="{{ route('order.checkout') }}" method="POST">
                            @csrf
                            <input type="hidden" name="total_price" value="{{ $total_price }}" />
                            <div class="col-12 mt-2 text-end pe-4">
                                <div>Total Price</div>
                                <h4>{{ 'Rp'.number_format($total_price, 0, ',', '.') }}</h4>
                                <button class="btn btn-success" type="submit">Checkout</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
