<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('created_at', 'DESC')->simplePaginate(10);

        return view('admins.products.index', [
            'products' => $products
        ]);
    }

    public function detail(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        if (!$product) {
            abort('404', 'Product not found');
        }

        return view('admins.products.detail', [
            'product' => $product
        ]);
    }

    public function create()
    {
        return view('admins.products.form');
    }
    
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "name" => ['required'],
            "type" => ['required'],
            "capital_price" => ['required'],
            "selling_price" => ['required'],
            "stock" => ['required'],
            "description" => ['required'],
            "product_image_url" => ['file', 'required']
        ]);

        if ($validate->fails()) {
            return redirect()->route('admin.product.create')->withErrors($validate)->withInput();
        }

        DB::beginTransaction();
        try {
            $url = null;
            if ($request->file('product_image_url')) {
                $targetPath = 'images/product';
                $directory = public_path($targetPath);
                if (!File::exists($directory)) {
                    File::makeDirectory($directory, 0775, true);
                }
                
                $filename = strtotime(date('Y-m-d H:i:s')).'.'.$request->file('product_image_url')->getClientOriginalExtension();
    
                $request->file('product_image_url')->move($directory, $filename);
                $url = asset($targetPath.'/'.$filename);
            }
    
            $product = Product::create([
                'name' => $request->name,
                'type' => $request->type,
                'capital_price' => $request->capital_price,
                'selling_price' => $request->selling_price,
                'stock' => $request->stock,
                'description' => $request->description,
                'product_image_url' => $url,
                'created_by_user_id' => auth()->user()->id
            ]);
            DB::commit();
    
            return redirect()->route('admin.product.create')->with('msg', 'Product created successfully. <a href="'.route('admin.product.detail', $product->id).'">Lihat Detail</a>');
        } catch (\Throwable $th) {
            DB::rollBack();
            $validate->getMessageBag()->add('system', $th->getMessage());
            
            return redirect()->route('admin.product.create')->withErrors($validate)->withInput();
        }
    }

    public function edit(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        if (!$product) {
            abort('404', 'Product not found');
        }

        return view('admins.products.form', [
            'product' => $product
        ]);
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "name" => ['required'],
            "type" => ['required'],
            "capital_price" => ['required'],
            "selling_price" => ['required'],
            "stock" => ['required'],
            "description" => ['required']
        ]);

        if ($validate->fails()) {
            return redirect()->route('admin.product.edit', $request->id)->withErrors($validate)->withInput();
        }

        DB::beginTransaction();
        try {
            $product = Product::where('id', $request->id)->first();
            if ($request->file('product_image_url')) {
                $targetPath = 'images/product';
                $directory = public_path($targetPath);
                if (!File::exists($directory)) {
                    File::makeDirectory($directory, 0775, true);
                }
                
                $filename = strtotime(date('Y-m-d H:i:s')).'.'.$request->file('product_image_url')->getClientOriginalExtension();
    
                $request->file('product_image_url')->move($directory, $filename);
                $url = asset($targetPath.'/'.$filename);

                $product->update([
                    'product_image_url' => $url
                ]);
            }
    
            $product->update([
                'name' => $request->name,
                'type' => $request->type,
                'capital_price' => $request->capital_price,
                'selling_price' => $request->selling_price,
                'stock' => $request->stock,
                'description' => $request->description,
                'created_by_user_id' => auth()->user()->id
            ]);
            DB::commit();
    
            return redirect()->route('admin.product.edit', $product->id)->with('msg', 'Product updated successfully. <a href="'.route('admin.product.detail', $product->id).'">Lihat Detail</a>');
        } catch (\Throwable $th) {
            DB::rollBack();
            $validate->getMessageBag()->add('system', $th->getMessage());
            
            return redirect()->route('admin.product.edit', $request->id)->withErrors($validate)->withInput();
        }
    }

    public function delete(Request $request)
    {
        Product::where('id', $request->id)->delete();

        return redirect()->route('admin.product.index')->with('msg', 'Product deleted successfully.');
    }
}
