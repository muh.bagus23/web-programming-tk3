<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::with('items')->simplePaginate();

        return view('admins.orders.index', [
            'orders' => $orders
        ]);
    }

    public function detail(Request $request)
    {
        $order = Order::with('items')->where('invoice_code', $request->code)->first();

        return view('admins.orders.detail', [
            'order' => $order
        ]);
    }

    public function confirm(Request $request)
    {
        DB::beginTransaction();
        try {
            $order = Order::where('id', $request->id)->first();

            foreach ($order->items as $key => $item) {
                $product = Product::where('id', $item->product_id)->first();
                $product->stock = $product->stock - $item->qty;
                $product->save();
            }

            $order->status = 'completed';
            $order->save();

            DB::commit();

            return redirect()->back();
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back();
        }
    }

    public function reject(Request $request)
    {
        $order = Order::where('id', $request->id)->firstOrFail();
        $order->status = 'rejected';
        $order->save();

        return redirect()->back();
    }
}
