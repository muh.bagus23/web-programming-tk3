<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('role', 'user')->simplePaginate(10);

        return view('admins.users.index', [
            'users' => $users
        ]);
    }

    public function detail(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        if (!$user) {
            abort('404', 'User not found');
        }

        return view('admins.users.detail', [
            'user' => $user
        ]);
    }

    public function create()
    {
        return view('admins.users.form');
    }
    
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "name" => ['required'],
            "birthdate" => ['required'],
            "address" => ['required'],
            "email" => ['required', 'unique:users,email'],
            "username" => ['required', 'unique:users,username'],
            "password" => ['required', 'confirmed'],
            "password_confirmation" => ['required'],
            "ktp_image_url" => ['file', 'required']
        ]);

        if ($validate->fails()) {
            return redirect()->route('admin.user.create')->withErrors($validate)->withInput();
        }

        DB::beginTransaction();
        try {
            $url = null;
            if ($request->file('ktp_image_url')) {
                $targetPath = 'images/user';
                $directory = public_path($targetPath);
                if (!File::exists($directory)) {
                    File::makeDirectory($directory, 0775, true);
                }
                
                $filename = strtotime(date('Y-m-d H:i:s')).'.'.$request->file('ktp_image_url')->getClientOriginalExtension();
    
                $request->file('ktp_image_url')->move($directory, $filename);
                $url = asset($targetPath.'/'.$filename);
            }
    
            $user = User::create([
                'email' => $request->email,
                'name' => $request->name,
                'birthdate' => $request->birthdate,
                'address' => $request->address,
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'ktp_image_url' => $url
            ]);
            DB::commit();
    
            return redirect()->route('admin.user.create')->with('msg', 'User created successfully. <a href="'.route('admin.user.detail', $user->id).'">Lihat Detail</a>');
        } catch (\Throwable $th) {
            DB::rollBack();
            $validate->getMessageBag()->add('system', $th->getMessage());
            
            return redirect()->route('admin.user.create')->withErrors($validate)->withInput();
        }
    }

    public function edit(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        if (!$user) {
            abort('404', 'User not found');
        }

        return view('admins.users.form', [
            'user' => $user
        ]);
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "name" => ['required'],
            "birthdate" => ['required'],
            "address" => ['required'],
            "email" => ['required'],
            "username" => ['required'],
            "password" => ['confirmed'],
            "password_confirmation" => ['required_with:password']
        ]);

        if ($validate->fails()) {
            return redirect()->route('admin.user.edit', $request->id)->withErrors($validate)->withInput();
        }

        DB::beginTransaction();
        try {
            $user = User::where('id', $request->id)->first();
            if ($request->file('ktp_image_url')) {
                $targetPath = 'images/user';
                $directory = public_path($targetPath);
                if (!File::exists($directory)) {
                    File::makeDirectory($directory, 0775, true);
                }
                
                $filename = strtotime(date('Y-m-d H:i:s')).'.'.$request->file('ktp_image_url')->getClientOriginalExtension();
    
                $request->file('ktp_image_url')->move($directory, $filename);
                $url = asset($targetPath.'/'.$filename);

                $user->update([
                    'ktp_image_url' => $url
                ]);
            }

            if ($request->password) {
                $user->update([
                    'password' => Hash::make($request->password)
                ]);
            }
    
            $user->update([
                'email' => $request->email,
                'name' => $request->name,
                'birthdate' => $request->birthdate,
                'address' => $request->address,
                'username' => $request->username
            ]);
            DB::commit();
    
            return redirect()->route('admin.user.edit', $user->id)->with('msg', 'User updated successfully. <a href="'.route('admin.user.detail', $user->id).'">Lihat Detail</a>');
        } catch (\Throwable $th) {
            DB::rollBack();
            $validate->getMessageBag()->add('system', $th->getMessage());
            
            return redirect()->route('admin.user.edit', $request->id)->withErrors($validate)->withInput();
        }
    }

    public function delete(Request $request)
    {
        User::where('id', $request->id)->delete();

        return redirect()->route('admin.user.index')->with('msg', 'User deleted successfully.');
    }
}
