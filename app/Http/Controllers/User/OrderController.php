<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\OrderItem;
use App\Models\Order;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    public function addToCart(Request $request)
    {
        $cart = Cart::where('user_id', auth()->user()->id)->where('product_id', $request->product_id)->first();

        Cart::updateOrCreate([
            'product_id' => $request->product_id,
            'user_id' => auth()->user()->id,
        ],[
            'qty' => $request->qty + ($cart->qty ?? 0)
        ]);

        return redirect()->back();
    }

    public function cart(Request $request)
    {
        $carts = Cart::has('product')->where('user_id', auth()->user()->id)->get();

        return view('cart', [
            'carts' => $carts
        ]);
    }

    public function checkout(Request $request)
    {
        DB::beginTransaction();
        try {
            $order = Order::create([
                'invoice_code' => strtoupper(Str::random(8)),
                'user_id' => auth()->user()->id,
                'total_price' => $request->total_price,
                'status' => 'waiting_confirmation'
            ]);
    
            $carts = Cart::where('user_id', auth()->user()->id)->get();
    
            foreach ($carts as $key => $cart) {
                if ($cart->qty > $cart->product->stock) {
                    throw new Exception("Stok tidak tersedia");
                }
                
                OrderItem::create([
                    'order_id' => $order->id,
                    'price' => $cart->product->selling_price,
                    'capital_price' => $cart->product->capital_price,
                    'product_id' => $cart->product_id,
                    'qty' => $cart->qty
                ]);
            }
    
            Cart::where('user_id', auth()->user()->id)->delete();
    
            DB::commit();

            return redirect()->route('order.detail', $order->invoice_code);
        } catch (\Throwable $th) {
            DB::rollBack();
            
            return redirect()->back();
        }
    }

    public function index(Request $request)
    {
        $orders = Order::with('items')->where('user_id', auth()->user()->id)->simplePaginate();

        return view('order', [
            'orders' => $orders
        ]);
    }
    
    public function detail(Request $request)
    {
        $order = Order::with('items')->where('user_id', auth()->user()->id)->where('invoice_code', $request->code)->first();

        return view('order-detail', [
            'order' => $order
        ]);
    }
}
